/**
 * Beaver: a GTK4-based Gopher client
 * Copyright (C) 2022 Hunter Turcin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <locale.h>
#include <stdlib.h>

#include <curl/curl.h>
#include <glib/gi18n-lib.h>
#include <gtk/gtk.h>

#include "beaver-window.h"

/**
 * Initialize the application.
 *
 * @param user_data unused
 */
static void app_activate(GApplication *app, gpointer user_data)
{
    GtkWidget *window = beaver_window_new(GTK_APPLICATION(app));

    gtk_widget_show(window);
}

int main(int argc, char *argv[])
{
    if (curl_global_init(CURL_GLOBAL_DEFAULT) != 0)
    {
        g_critical("curl_global_init returned non-zero, exiting");

        return EXIT_FAILURE;
    }

    setlocale(LC_ALL, "");
    bindtextdomain(GETTEXT_PACKAGE, PREFIX "/share/locale");
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);

    GtkApplication *app = gtk_application_new("net.huntertur.Beaver", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(app_activate), NULL);
    int stat = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);

    curl_global_cleanup();

    return stat;
}
