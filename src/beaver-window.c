/**
 * Beaver: a GTK4-based Gopher client
 * Copyright (C) 2022 Hunter Turcin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>

#include <glib/gi18n-lib.h>
#include <gtk/gtk.h>

#include "beaver-list-box-row.h"
#include "beaver-window.h"
#include "gopher-menu.h"
#include "network.h"
#include "offload.h"

struct _BeaverWindow
{
    GtkApplicationWindow parent;

    /**
     * A back-navigating GtkButton.
     */
    GtkWidget *back;

    /**
     * A GtkListBox.
     */
    GtkWidget *body;

    /**
     * A URI-entering GtkEntry.
     */
    GtkWidget *entry;

    /**
     * A forward-navigating GtkButton.
     */
    GtkWidget *forward;

    /**
     * An array of historical URIs as C strings.
     */
    GArray *history;

    /**
     * The user's current index in the history.
     */
    guint history_index;

    /**
     * A refresh button that re-fetches the current URI in the history.
     */
    GtkWidget *refresh;
};

G_DEFINE_TYPE(BeaverWindow, beaver_window, GTK_TYPE_APPLICATION_WINDOW);

/**
 * Display an error dialog.
 */
static void display_error(GtkWindow *window, const char *message)
{
    GtkWidget *dialog = gtk_message_dialog_new(
        window,
        GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_ERROR,
        GTK_BUTTONS_OK,
        "%s",
        message
    );

    g_signal_connect(dialog, "response", G_CALLBACK(gtk_window_destroy), NULL);

    gtk_widget_show(dialog);
}

/**
 * Display the about dialog.
 *
 * @param user_data parent GtkWindow
 */
static void about_activate(GSimpleAction *action, GVariant *param, gpointer user_data)
{
    const char *authors[] = {
        "Hunter Turcin <huntertur@gmail.com>",
        NULL
    };

    GtkWindow *window;

    window = GTK_WINDOW(user_data);

    gtk_show_about_dialog(
        window,
        "authors", authors,
        "comments", _("A GTK4-based Gopher client"),
        "copyright", _("Copyright 2022 Hunter Turcin"),
        "license-type", GTK_LICENSE_GPL_3_0,
        "logo-icon-name", "web-browser-symbolic",
        "program-name", "Beaver",
        "website", "https://gitlab.com/huntertur/beaver",
        NULL
    );
}

/**
 * Fetch the requested Gopher resource and display it.
 *
 * @param update_history FALSE if displaying historical data, TRUE if otherwise
 */
static void navigate(BeaverWindow *self, gboolean update_history)
{
    GtkEntryBuffer *entry_buffer;
    GtkWidget *row;
    const char *entry_text;
    NetworkResult result;
    GopherItem **items;
    GopherItem **items_iter;
    GopherItem *item;

    entry_buffer = gtk_entry_get_buffer(GTK_ENTRY(self->entry));
    entry_text = gtk_entry_buffer_get_text(entry_buffer);

    result = fetch_uri(entry_text);

    if (result.is_error)
    {
        display_error(GTK_WINDOW(self), result.response_or_error);

        return;
    }

    if (update_history)
    {
        if (self->history_index != -1)
        {
            gtk_widget_set_sensitive(self->back, TRUE);
        }

        gtk_widget_set_sensitive(self->forward, FALSE);
        gtk_widget_set_sensitive(self->refresh, TRUE);

        self->history_index++;

        /* If the user is navigating to a new URI from the past,
           remove all future URIs from the history. */
        for (int i = self->history_index; i + 1 < self->history->len; i++)
        {
            g_free(g_array_index(self->history, char *, i));
        }

        g_array_set_size(self->history, self->history_index + 1);
        const char *entry_text_copy = g_strdup(entry_text);
        g_array_insert_val(self->history, self->history_index, entry_text_copy);
    }

    while ((row = GTK_WIDGET(gtk_list_box_get_row_at_index(GTK_LIST_BOX(self->body), 0))) != NULL)
    {
        gtk_list_box_remove(GTK_LIST_BOX(self->body), row);
    }

    items = gopher_item_array_new_from_raw(result.response_or_error);
    items_iter = items;

    while ((item = items_iter++[0]) != NULL)
    {
        GtkWidget *row = beaver_list_box_row_new(item);

        gtk_list_box_append(GTK_LIST_BOX(self->body), row);
    }

    g_free(items);
}

/**
 * Fetch the requested Gopher resource and display it.
 *
 * @param user_data BeaverWindow
 */
static void entry_activate(GtkEntry *self, gpointer user_data)
{
    GtkEntryBuffer *buffer = gtk_entry_get_buffer(self);
    const char *address = gtk_entry_buffer_get_text(buffer);

    if (g_strstr_len(address, 9, "gopher://") == NULL)
    {
        char *new_address = g_strconcat("gopher://", address, NULL);
        gtk_entry_buffer_set_text(buffer, new_address, -1);
        g_free(new_address);
    }

    navigate(BEAVER_WINDOW(user_data), TRUE);
}

/**
 * Re-fetch the current Gopher URI in the history.
 *
 * @param user_data BeaverWindow
 */
static void refresh_clicked(GtkButton *refresh, gpointer user_data)
{
    BeaverWindow *self = BEAVER_WINDOW(user_data);

    GtkEntry *entry = GTK_ENTRY(self->entry);
    GtkEntryBuffer *buffer = gtk_entry_get_buffer(entry);
    const char *uri = g_array_index(self->history, const char *, self->history_index);
    gtk_entry_buffer_set_text(buffer, uri, -1);

    navigate(self, FALSE);
}

/**
 * Go back one Gopher URI in the history.
 *
 * @param user_data BeaverWindow
 */
static void back_clicked(GtkButton *back, gpointer user_data)
{
    BeaverWindow *self = BEAVER_WINDOW(user_data);

    self->history_index--;

    if (self->history_index == 0)
    {
        gtk_widget_set_sensitive(self->back, FALSE);
    }

    gtk_widget_set_sensitive(self->forward, TRUE);

    GtkEntry *entry = GTK_ENTRY(self->entry);
    GtkEntryBuffer *buffer = gtk_entry_get_buffer(entry);
    const char *uri = g_array_index(self->history, const char *, self->history_index);
    gtk_entry_buffer_set_text(buffer, uri, -1);

    navigate(self, FALSE);
}

/**
 * Go forward one Gopher URI in the history.
 *
 * @param user_data BeaverWindow
 */
static void forward_clicked(GtkButton *forward, gpointer user_data)
{
    BeaverWindow *self = BEAVER_WINDOW(user_data);

    self->history_index++;

    if (self->history_index + 2 == self->history->len)
    {
        gtk_widget_set_sensitive(self->forward, FALSE);
    }

    gtk_widget_set_sensitive(self->back, TRUE);

    GtkEntry *entry = GTK_ENTRY(self->entry);
    GtkEntryBuffer *buffer = gtk_entry_get_buffer(entry);
    const char *uri = g_array_index(self->history, const char *, self->history_index);
    gtk_entry_buffer_set_text(buffer, uri, -1);

    navigate(self, FALSE);
}

static void display_item(BeaverWindow *self, GopherItem *item)
{
    char *uri = gopher_item_to_uri(item);

    if (gopher_item_is_url(item))
    {
        open_viewer_for_url(uri);
        goto cleanup;
    }

    NetworkResult result = fetch_uri(uri);

    if (result.is_error)
    {
        display_error(GTK_WINDOW(self), result.response_or_error);
        goto cleanup;
    }

    const char *filename = g_strrstr(item->selector, "/");
    filename = filename == NULL ? item->selector : filename + 1;
    open_viewer_for_content(filename, result.response_or_error, result.response_size);
cleanup:
    g_free(uri);
}

/**
 * Attempt to navigate to the next submenu.
 *
 * @param raw_row the activated BeaverListBoxRow
 * @param user_data the parent BeaverWindow
 */
static void row_activated(GtkListBox *box, GtkListBoxRow *raw_row, gpointer user_data)
{
    BeaverWindow *self = BEAVER_WINDOW(user_data);
    BeaverListBoxRow *row = BEAVER_LIST_BOX_ROW(raw_row);
    GopherItem *item = beaver_list_box_row_get_item(row);

    switch (item->type)
    {
    case TYPE_FILE:
    case TYPE_BINHEX:
    case TYPE_DOS_ARCHIVE:
    case TYPE_UUENCODED:
    case TYPE_BINARY:
    case TYPE_GIF:
    case TYPE_IMAGE:
    case TYPE_PNG:
    case TYPE_HTML:
    case TYPE_DOCUMENT:
    case TYPE_SOUND:
    case TYPE_MAIL:
        display_item(self, item);
        break;
    case TYPE_DIRECTORY:; /* semicolon needed due to succeeding declaration */
        GtkEntryBuffer *buffer = gtk_entry_get_buffer(GTK_ENTRY(self->entry));
        char *uri = gopher_item_to_uri(item);
        gtk_entry_buffer_set_text(buffer, uri, -1);
        navigate(self, TRUE);
        g_free(uri);
        break;
    default:
        display_error(GTK_WINDOW(user_data), _("Not yet supported"));
    }

}

/**
 * Create the menu for the header menu button.
 *
 * @return header menu
 */
static GMenu *make_header_menu()
{
    GMenu *menu;

    menu = g_menu_new();
    g_menu_append(menu, _("About Beaver"), "win.about");

    return menu;
}

static void beaver_window_init(BeaverWindow *self)
{
    const GActionEntry window_actions[] = {
        { "about", about_activate, NULL, NULL, NULL },
    };

    self->back = gtk_button_new();
    self->body = gtk_list_box_new();
    self->entry = gtk_entry_new();
    self->forward = gtk_button_new();
    self->history = g_array_new(FALSE, FALSE, sizeof(char *));
    self->history_index = -1;
    self->refresh = gtk_button_new();
    GtkWidget *header_bar = gtk_header_bar_new();
    GtkWidget *menu_button = gtk_menu_button_new();
    GtkWidget *navigation = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    GtkWidget *scrolled_window = gtk_scrolled_window_new();
    GMenu *menu = make_header_menu();

    g_action_map_add_action_entries(G_ACTION_MAP(self), window_actions, G_N_ELEMENTS(window_actions), self);
    gtk_window_set_title(GTK_WINDOW(self), "Beaver");

    gtk_window_set_titlebar(GTK_WINDOW(self), header_bar);

    gtk_menu_button_set_menu_model(GTK_MENU_BUTTON(menu_button), G_MENU_MODEL(menu));
    gtk_menu_button_set_icon_name(GTK_MENU_BUTTON(menu_button), "open-menu-symbolic");

    gtk_header_bar_pack_end(GTK_HEADER_BAR(header_bar), menu_button);

    gtk_scrolled_window_set_child(GTK_SCROLLED_WINDOW(scrolled_window), self->body);

    gtk_window_set_child(GTK_WINDOW(self), scrolled_window);
    g_signal_connect(self->body, "row-activated", G_CALLBACK(row_activated), self);

    gtk_widget_set_hexpand(self->entry, TRUE);
    gtk_entry_set_placeholder_text(GTK_ENTRY(self->entry), _("Enter Gopher address"));
    g_signal_connect(self->entry, "activate", G_CALLBACK(entry_activate), self);

    gtk_header_bar_set_title_widget(GTK_HEADER_BAR(header_bar), self->entry);
    gtk_widget_grab_focus(self->entry);

    gtk_style_context_add_class(gtk_widget_get_style_context(navigation), "linked");
    gtk_header_bar_pack_start(GTK_HEADER_BAR(header_bar), navigation);

    gtk_button_set_icon_name(GTK_BUTTON(self->back), "go-previous-symbolic");
    gtk_widget_set_sensitive(self->back, FALSE);
    g_signal_connect(self->back, "clicked", G_CALLBACK(back_clicked), self);
    gtk_box_append(GTK_BOX(navigation), self->back);

    gtk_button_set_icon_name(GTK_BUTTON(self->forward), "go-next-symbolic");
    gtk_widget_set_sensitive(self->forward, FALSE);
    g_signal_connect(self->forward, "clicked", G_CALLBACK(forward_clicked), self);
    gtk_box_append(GTK_BOX(navigation), self->forward);

    gtk_button_set_icon_name(GTK_BUTTON(self->refresh), "view-refresh-symbolic");
    gtk_widget_set_sensitive(self->refresh, FALSE);
    g_signal_connect(self->refresh, "clicked", G_CALLBACK(refresh_clicked), self);
    gtk_header_bar_pack_start(GTK_HEADER_BAR(header_bar), self->refresh);
}

static void beaver_window_class_init(BeaverWindowClass *class)
{
}

GtkWidget *beaver_window_new(GtkApplication *app)
{
    return g_object_new(
        BEAVER_TYPE_WINDOW,
        "application", app,
        "default-height", 480,
        "default-width", 640,
        "icon-name", "web-browser-symbolic",
        NULL
    );
}
