/**
 * Beaver: a GTK4-based Gopher client
 * Copyright (C) 2022 Hunter Turcin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <pthread.h>
#include <spawn.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

#include <glib.h>

#include "offload.h"

extern char **environ;

/**
 * Open the viewer at the temporary file, then delete it once the viewer exits.
 */
static void *open_viewer_and_cleanup(void *path_uncasted)
{
    char *path = (char *)path_uncasted;
    /* Launching xdg-open normally is non-blocking, while launching it in a
       subshell is blocking. The blocking behavior is desired here. */
    char *quoted_path = g_shell_quote(path);
    char *command = g_strconcat("$(/usr/bin/xdg-open ", quoted_path, ")", NULL);
    char *argv[] = { "/bin/sh", "-c", command, NULL };
    pid_t pid;
    posix_spawn(&pid, argv[0], NULL, NULL, argv, environ);
    g_free(quoted_path);
    g_free(command);

    waitpid(pid, NULL, 0);

    /* Some web browsers like Firefox will, if an instance is already open,
       have the command terminate immediately, before the new tab in the
       existing instance is opened. So, unfortunately, a sleep is needed. */
    sleep(5);

    unlink(path);
    char *end = g_strrstr(path, "/");
    size_t directory_length = end - path;
    char *directory = g_strndup(path, directory_length);
    rmdir(directory);
    g_free(directory);

    return NULL;
}

void open_viewer_for_content(const char *filename, const void *data, size_t size)
{
    char template[] = "/tmp/Beaver-XXXXXX";
    char *path = g_strconcat(mkdtemp(template), "/", filename, NULL);
    FILE *file = fopen(path, "w+");
    fwrite(data, 1, size, file);
    fclose(file);

    pthread_t thread;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_create(&thread, &attr, open_viewer_and_cleanup, path);
}

void open_viewer_for_url(char *url)
{
    pid_t pid;
    char *argv[] = { "/usr/bin/xdg-open", url, NULL };
    posix_spawn(&pid, argv[0], NULL, NULL, argv, environ);
}
