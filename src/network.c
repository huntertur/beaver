/**
 * Beaver: a GTK4-based Gopher client
 * Copyright (C) 2022 Hunter Turcin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <curl/curl.h>
#include <glib/gi18n-lib.h>
#include <glib.h>

#include "network.h"

NetworkResult fetch_uri(const char *uri)
{
    NetworkResult result = { TRUE, 0, NULL };
    char response_buffer[1024];
    size_t response_buffer_len;
    CURL *curl;
    CURLcode curl_code;
    FILE *stream;
    GString *response;

    if ((curl = curl_easy_init()) == NULL)
    {
        result.response_or_error = _("curl_easy_init failed");
        return result;
    }

    if ((stream = tmpfile()) == NULL)
    {
        result.response_or_error = strerror(errno);
        goto cleanup_curl;
    }

    curl_easy_setopt(curl, CURLOPT_URL, uri);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, stream);
    curl_code = curl_easy_perform(curl);

    if (curl_code != CURLE_OK)
    {
        result.response_or_error = curl_easy_strerror(curl_code);
        goto cleanup_stream;
    }

    response = g_string_new(NULL);
    rewind(stream);

    while (!feof(stream))
    {
        response_buffer_len = fread(response_buffer, 1, 1024, stream);

        if (ferror(stream))
        {
            result.response_or_error = _("Error flag set on stream");
            g_string_free(response, TRUE);
            goto cleanup_stream;
        }

        response = g_string_append_len(response, response_buffer, response_buffer_len);
    }

    result.is_error = FALSE;
    result.response_size = response->len;
    result.response_or_error = g_string_free(response, FALSE);

cleanup_stream:
    fclose(stream);
cleanup_curl:
    curl_easy_cleanup(curl);

    return result;
}
