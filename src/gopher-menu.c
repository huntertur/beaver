/**
 * Beaver: a GTK4-based Gopher client
 * Copyright (C) 2022 Hunter Turcin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <string.h>

#include <glib.h>

#include "gopher-menu.h"

static size_t array_bytes(size_t items)
{
    return sizeof(void *) * (items + 1);
}

GopherItem **gopher_item_array_new_from_raw(const char *raw)
{
    GopherItem **array;
    size_t size;
    const char *start;
    const char *end;

    size = 0;
    array = g_malloc(array_bytes(0));
    array[0] = NULL;

    start = raw;

    while ((end = strstr(start, "\r\n")) != NULL)
    {
        const char *line = g_strndup(start, end - start);

        GopherItem *item = gopher_item_new_from_raw(line);

        if (item == NULL)
        {
            break;
        }

        array[size] = item;
        size++;
        array = g_realloc(array, array_bytes(size));
        array[size] = NULL;

        start = end + 2;
    }

    return array;
}

GopherItem *gopher_item_new(GopherItemType type, const char *display, const char *selector, const char *hostname, int port)
{
    return gopher_item_new_moving(type, g_strdup(display), g_strdup(selector), g_strdup(hostname), port);
}

GopherItem *gopher_item_new_moving(GopherItemType type, const char *display, const char *selector, const char *hostname, int port)
{
    GopherItem *item;

    item = g_malloc(sizeof(GopherItem));

    item->type = type;
    item->display = display;
    item->selector = selector;
    item->hostname = hostname;
    item->port = port;

    return item;
}

GopherItem *gopher_item_new_from_raw(const char *raw_base)
{
    size_t len;

    char type;
    size_t display_len = 0;
    size_t selector_len = 0;
    size_t hostname_len = 0;
    int port = -1;
    int tab_count = 0;

    const char *display;
    const char *selector;
    const char *hostname;

    const char *raw;

    len = strlen(raw_base);

    /* First, check if the line is malformed. */
    if (len < 5)
    {
        return NULL;
    }

    for (int i = 0; i < len; i++)
    {
        if (raw_base[i] == '\t')
        {
            tab_count++;
        }
    }

    if (tab_count != 3)
    {
        return NULL;
    }

    /* Now, parse. */
    raw = raw_base;

    type = raw[0];

    raw++;

    while (*raw++ != '\t')
    {
        display_len++;
    }

    while (*raw++ != '\t')
    {
        selector_len++;
    }

    while (*raw++ != '\t')
    {
        hostname_len++;
    }

    port = atoi(raw);

    if (port == 0)
    {
        return NULL;
    }

    display = g_strndup(raw_base + 1, display_len);
    selector = g_strndup(raw_base + 1 + display_len + 1, selector_len);
    hostname = g_strndup(raw_base + 1 + display_len + 1 + selector_len + 1, hostname_len);

    return gopher_item_new_moving(type, display, selector, hostname, port);
}

void gopher_item_free(GopherItem *item)
{
    g_free((void *)item->display);
    g_free((void *)item->selector);
    g_free((void *)item->hostname);

    g_free(item);
}

char *gopher_item_to_uri(GopherItem *self)
{
    if (gopher_item_is_url(self))
    {
        return g_strdup(self->selector + 4);
    }

    const char *path_unescaped = self->selector[0] == '/' ? self->selector + 1 : self->selector;
    size_t path_unescaped_length = strlen(path_unescaped);
    int spaces = 0;

    for (int i = 0; i < path_unescaped_length; i++)
    {
        if (path_unescaped[i] == ' ')
        {
            spaces++;
        }
    }

    /* each space is expanded from 1 to 3 bytes, and need byte for the NULL */
    char *path = g_malloc(path_unescaped_length + 2 * spaces + 1);

    int i;
    int j;

    for (i = 0, j = 0; i < path_unescaped_length; i++, j++)
    {
        if (path_unescaped[i] == ' ')
        {
            path[j] = '%';
            path[j + 1] = '2';
            path[j + 2] = '0';
            j += 2;
        }
        else
        {
            path[j] = path_unescaped[i];
        }
    }

    path[j] = '\0';

    char *port_string = "";

    if (self->port != 70)
    {
        port_string = g_strdup_printf(":%d", self->port);
    }

    char *uri = g_strdup_printf(
        "gopher://%s%s/%c/%s",
        self->hostname,
        port_string,
        self->type,
        path
    );

    if (self->port != 70)
    {
        g_free(port_string);
    }

    g_free(path);

    return uri;
}

bool gopher_item_is_url(GopherItem *self)
{
    return g_str_has_prefix(self->selector, "URL:");
}
