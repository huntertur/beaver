/**
 * Beaver: a GTK4-based Gopher client
 * Copyright (C) 2022 Hunter Turcin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <glib/gi18n-lib.h>
#include <gtk/gtk.h>

#include "beaver-list-box-row.h"
#include "gopher-menu.h"

struct _BeaverListBoxRow
{
    GtkListBoxRow parent;
    GopherItem *item;
};

G_DEFINE_TYPE(BeaverListBoxRow, beaver_list_box_row, GTK_TYPE_LIST_BOX_ROW);

static void beaver_list_box_row_finalize(GObject *self)
{
    gopher_item_free(BEAVER_LIST_BOX_ROW(self)->item);

    G_OBJECT_CLASS(beaver_list_box_row_parent_class)->finalize(G_OBJECT(self));
}

static void beaver_list_box_row_init(BeaverListBoxRow *row)
{
}

static void beaver_list_box_row_class_init(BeaverListBoxRowClass *class)
{
    GObjectClass *object_class = G_OBJECT_CLASS(class);

    object_class->finalize = beaver_list_box_row_finalize;
}

/**
 * Make a GtkImage representing the given Gopher menu type.
 *
 * @return a GtkImage
 */
static GtkWidget *make_icon_for_type(GopherItemType type)
{
    const char *name;
    const char *description;
    GtkWidget *image;

    switch (type)
    {
    case TYPE_FILE:
        name = "document-open-symbolic";
        description = _("Plain text");
        break;
    case TYPE_DIRECTORY:
        name = "folder-open-symbolic";
        description = _("Directory");
        break;
    case TYPE_CSO_PHONEBOOK:
        name = "phone-symbolic";
        description = _("CSO phonebook");
        break;
    case TYPE_ERROR:
        name = "dialog-error-symbolic";
        description = _("Error");
        break;
    case TYPE_BINHEX:
        name = "computer-apple-ipad-symbolic";
        description = _("BinHex-encoded file");
        break;
    case TYPE_DOS_ARCHIVE:
        name = "computer-symbolic";
        description = _("DOS archive");
        break;
    case TYPE_UUENCODED:
        name = "media-zip-symbolic";
        description = _("<tt>uuencode</tt>d file");
        break;
    case TYPE_INDEX_SEARCH:
        name = "folder-saved-search-symbolic";
        description = _("Index search");
        break;
    case TYPE_TELNET:
        name = "utilities-terminal-symbolic";
        description = _("Telnet connection");
        break;
    case TYPE_BINARY:
        name = "application-x-executable-symbolic";
        description = _("Binary file");
        break;
    case TYPE_REDUNDANT_SERVER:
        name = "network-server-symbolic";
        description = _("Redundant server");
        break;
    case TYPE_TN3270:
        name = "input-keyboard-symbolic";
        description = _("<tt>tn3270</tt> connection");
        break;
    case TYPE_GIF:
        name = "video-x-generic-symbolic";
        description = _("GIF image");
        break;
    case TYPE_IMAGE:
        name = "image-x-generic-symbolic";
        description = _("Image");
        break;
    case TYPE_INFO:
        name = NULL;
        description = NULL;
        break;
    case TYPE_PNG:
        name = "insert-image-symbolic";
        description = _("PNG image");
        break;
    case TYPE_HTML:
        name = "web-browser-symbolic";
        description = _("HTML document");
        break;
    case TYPE_DOCUMENT:
        name = "x-office-document-symbolic";
        description = _("Document");
        break;
    case TYPE_SOUND:
        name = "audio-x-generic-symbolic";
        description = _("Sound");
        break;
    case TYPE_MAIL:
        name = "mail-unread-symbolic";
        description = _("Email message");
        break;
    default:
        name = "dialog-question-symbolic";
        description = _("Unknown");
        g_warning("unknown menu item type: %c", type);
    }

    image = gtk_image_new_from_icon_name(name);
    gtk_widget_set_tooltip_markup(image, description);

    return image;
}

GtkWidget *beaver_list_box_row_new(GopherItem *item)
{
    g_return_val_if_fail(item != NULL, NULL);

    BeaverListBoxRow *self = g_object_new(BEAVER_TYPE_LIST_BOX_ROW, NULL);

    self->item = item;

    GtkWidget *box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 6);

    GtkWidget *image = make_icon_for_type(item->type);

    GtkWidget *label = gtk_label_new(NULL);
    char *valid_display = g_utf8_make_valid(item->display, -1);
    char *markup = g_markup_printf_escaped("<tt>%s</tt>", valid_display);
    gtk_label_set_markup(GTK_LABEL(label), markup);
    gtk_widget_set_halign(label, GTK_ALIGN_START);
    char *uri = gopher_item_to_uri(item);
    gtk_widget_set_tooltip_text(label, uri);

    gtk_box_append(GTK_BOX(box), image);
    gtk_box_append(GTK_BOX(box), label);

    gtk_list_box_row_set_child(GTK_LIST_BOX_ROW(self), box);
    gboolean can_use = item->type != TYPE_INFO && item->type != TYPE_ERROR;
    gtk_list_box_row_set_activatable(GTK_LIST_BOX_ROW(self), can_use);
    gtk_list_box_row_set_selectable(GTK_LIST_BOX_ROW(self), can_use);

    g_free(uri);
    g_free(valid_display);
    g_free(markup);

    return GTK_WIDGET(self);
}

GopherItem *beaver_list_box_row_get_item(BeaverListBoxRow *self)
{
    g_return_val_if_fail(BEAVER_IS_LIST_BOX_ROW(self), NULL);

    return self->item;
}
