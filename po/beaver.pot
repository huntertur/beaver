# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Hunter Turcin
# This file is distributed under the same license as the beaver package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: beaver\n"
"Report-Msgid-Bugs-To: https://gitlab.com/huntertur/beaver/-/issues\n"
"POT-Creation-Date: 2022-01-22 14:21-0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/beaver-list-box-row.c:65
msgid "Plain text"
msgstr ""

#: src/beaver-list-box-row.c:69
msgid "Directory"
msgstr ""

#: src/beaver-list-box-row.c:73
msgid "CSO phonebook"
msgstr ""

#: src/beaver-list-box-row.c:77
msgid "Error"
msgstr ""

#: src/beaver-list-box-row.c:81
msgid "BinHex-encoded file"
msgstr ""

#: src/beaver-list-box-row.c:85
msgid "DOS archive"
msgstr ""

#: src/beaver-list-box-row.c:89
msgid "<tt>uuencode</tt>d file"
msgstr ""

#: src/beaver-list-box-row.c:93
msgid "Index search"
msgstr ""

#: src/beaver-list-box-row.c:97
msgid "Telnet connection"
msgstr ""

#: src/beaver-list-box-row.c:101
msgid "Binary file"
msgstr ""

#: src/beaver-list-box-row.c:105
msgid "Redundant server"
msgstr ""

#: src/beaver-list-box-row.c:109
msgid "<tt>tn3270</tt> connection"
msgstr ""

#: src/beaver-list-box-row.c:113
msgid "GIF image"
msgstr ""

#: src/beaver-list-box-row.c:117
msgid "Image"
msgstr ""

#: src/beaver-list-box-row.c:125
msgid "PNG image"
msgstr ""

#: src/beaver-list-box-row.c:129
msgid "HTML document"
msgstr ""

#: src/beaver-list-box-row.c:133
msgid "Document"
msgstr ""

#: src/beaver-list-box-row.c:137
msgid "Sound"
msgstr ""

#: src/beaver-list-box-row.c:141
msgid "Email message"
msgstr ""

#: src/beaver-list-box-row.c:145
msgid "Unknown"
msgstr ""

#: src/beaver-window.c:109
msgid "A GTK4-based Gopher client"
msgstr ""

#: src/beaver-window.c:110
msgid "Copyright 2022 Hunter Turcin"
msgstr ""

#: src/beaver-window.c:338
msgid "Not yet supported"
msgstr ""

#: src/beaver-window.c:353
msgid "About Beaver"
msgstr ""

#: src/beaver-window.c:393
msgid "Enter Gopher address"
msgstr ""

#: src/network.c:40
msgid "curl_easy_init failed"
msgstr ""

#: src/network.c:69
msgid "Error flag set on stream"
msgstr ""
