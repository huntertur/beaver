# Beaver: a GTK4-based Gopher client

Copyright (C) 2022 Hunter Turcin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Operation

Navigating between Gopher menus is done within the program. Back-and-forward
history navigation is available.

When a file is downloaded over Gopher, it will be opened in your preferred
viewer using `xdg-open`.

## Why?

I wanted to improve my GUI and C programming skills.

## Building

Make sure you have these prerequisites installed:

  * gtk4
  * libcurl
  * xdg-open

Then:

```sh
meson setup builddir
cd builddir
meson compile beaver
```

To run, execute `./src/beaver`.

## Testing

First, execute the commands from the Building section.

To run the unit test suite, execute `meson test`.

## Installing

First, execute the commands from the Building section.

To use the default install directory (likely `/usr/local`), simply execute
`meson install`.

To install to another directory, set the installation prefix with
`meson configure --prefix PREFIX`, substituting `PREFIX` with your preferred
prefix, then run `meson install`. A good choice is `$HOME/.local`.
