/**
 * Beaver: a GTK4-based Gopher client
 * Copyright (C) 2022 Hunter Turcin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <glib.h>

#include "gopher-menu.h"

static void test_invalid_empty()
{
    const char *line = "";

    GopherItem *item = gopher_item_new_from_raw(line);

    g_assert_null(item);
}

static void test_invalid_short_1()
{
    const char *line = "i";

    GopherItem *item = gopher_item_new_from_raw(line);

    g_assert_null(item);
}

static void test_invalid_short_2()
{
    const char *line = "iA";

    GopherItem *item = gopher_item_new_from_raw(line);

    g_assert_null(item);
}

static void test_invalid_short_3()
{
    const char *line = "iA\tB";

    GopherItem *item = gopher_item_new_from_raw(line);

    g_assert_null(item);
}

static void test_invalid_short_4()
{
    const char *line = "iA\tB\tC\t";

    GopherItem *item = gopher_item_new_from_raw(line);

    g_assert_null(item);
}

static void test_invalid_too_few_fields()
{
    const char *line = "iOne\tTwo\t60\r\n";

    GopherItem *item = gopher_item_new_from_raw(line);

    g_assert_null(item);
}

static void test_invalid_too_many_fields()
{
    const char *line = "iOne\tTwo\tThree\tFour\t60\r\n";

    GopherItem *item = gopher_item_new_from_raw(line);

    g_assert_null(item);
}

static void test_uri_has_slash_prefix()
{
    GopherItem *item = gopher_item_new('1', "Display", "/item", "host", 70);

    char *uri = gopher_item_to_uri(item);

    g_assert_cmpstr(uri, ==, "gopher://host/1/item");
}

static void test_uri_has_port()
{
    GopherItem *item = gopher_item_new('0', "Display", "item", "host", 80);

    char *uri = gopher_item_to_uri(item);

    g_assert_cmpstr(uri, ==, "gopher://host:80/0/item");
}

static void test_uri_has_spaces()
{
    GopherItem *item = gopher_item_new('1', "Display Name", "The Selector", "host", 70);

    char *uri = gopher_item_to_uri(item);

    g_assert_cmpstr(uri, ==, "gopher://host/1/The%20Selector");
}

static void test_uri_is_empty()
{
    GopherItem *item = gopher_item_new(' ', "", "", "", 0);

    char *uri = gopher_item_to_uri(item);

    g_assert_cmpstr(uri, ==, "gopher://:0/ /");
}

static void test_valid_empty_fields()
{
    const char *line = "i\t\t\t1\r\n";

    GopherItem *item = gopher_item_new_from_raw(line);

    g_assert_nonnull(item);
    g_assert_true(item->type == 'i');
    g_assert_cmpstr(item->display, ==, "");
    g_assert_cmpstr(item->selector, ==, "");
    g_assert_cmpstr(item->hostname, ==, "");
    g_assert_true(item->port == 1);
}

static void test_valid_has_spaces()
{
    const char *line = "i Dis play \t Sel ector \t Host name \t 70 \r\n";

    GopherItem *item = gopher_item_new_from_raw(line);

    g_assert_nonnull(item);
    g_assert_true(item->type == 'i');
    g_assert_cmpstr(item->display, ==, " Dis play ");
    g_assert_cmpstr(item->selector, ==, " Sel ector ");
    g_assert_cmpstr(item->hostname, ==, " Host name ");
    g_assert_true(item->port == 70);
}

static void test_valid_no_newline()
{
    const char *line = "1One\tTwo\tThree\t4";

    GopherItem *item = gopher_item_new_from_raw(line);

    g_assert_nonnull(item);
    g_assert_true(item->type == '1');
    g_assert_cmpstr(item->display, ==, "One");
    g_assert_cmpstr(item->selector, ==, "Two");
    g_assert_cmpstr(item->hostname, ==, "Three");
    g_assert_true(item->port == 4);
}

static void test_valid_no_spaces()
{
    const char *line = "iDisplay\tSelector\tHostname\t70\r\n";

    GopherItem *item = gopher_item_new_from_raw(line);

    g_assert_nonnull(item);
    g_assert_true(item->type == 'i');
    g_assert_cmpstr(item->display, ==, "Display");
    g_assert_cmpstr(item->selector, ==, "Selector");
    g_assert_cmpstr(item->hostname, ==, "Hostname");
    g_assert_true(item->port == 70);
}

int main(int argc, char *argv[])
{
    g_test_init(&argc, &argv, NULL);

    g_test_add_func("/gopher-menu/invalid-empty", test_invalid_empty);
    g_test_add_func("/gopher-menu/invalid-short-1", test_invalid_short_1);
    g_test_add_func("/gopher-menu/invalid-short-2", test_invalid_short_2);
    g_test_add_func("/gopher-menu/invalid-short-3", test_invalid_short_3);
    g_test_add_func("/gopher-menu/invalid-short-4", test_invalid_short_4);
    g_test_add_func("/gopher-menu/invalid-too-few-fields", test_invalid_too_few_fields);
    g_test_add_func("/gopher-menu/invalid-too-many-fields", test_invalid_too_many_fields);
    g_test_add_func("/gopher-menu/uri-has-slash-prefix", test_uri_has_slash_prefix);
    g_test_add_func("/gopher-menu/uri-has-port", test_uri_has_port);
    g_test_add_func("/gopher-menu/uri-has-spaces", test_uri_has_spaces);
    g_test_add_func("/gopher-menu/uri-is-empty", test_uri_is_empty);
    g_test_add_func("/gopher-menu/valid-empty-fields", test_valid_empty_fields);
    g_test_add_func("/gopher-menu/valid-no-newline", test_valid_no_newline);
    g_test_add_func("/gopher-menu/valid-no-spaces", test_valid_no_spaces);
    g_test_add_func("/gopher-menu/valid-has-spaces", test_valid_has_spaces);

    return g_test_run();
}
