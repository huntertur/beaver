/**
 * Beaver: a GTK4-based Gopher client
 * Copyright (C) 2022 Hunter Turcin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NETWORK_H
#define NETWORK_H

struct _NetworkResult
{
    gboolean is_error;
    size_t response_size;
    const char *response_or_error;
};

/**
 * Structure for returning either a valid response or an error message.
 */
typedef struct _NetworkResult NetworkResult;

/**
 * Make a request to the given URI and read the entire response.
 *
 * @return the entire response, or an error message if failure
 */
NetworkResult fetch_uri(const char *uri);

#endif
