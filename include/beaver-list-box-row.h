/**
 * Beaver: a GTK4-based Gopher client
 * Copyright (C) 2022 Hunter Turcin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef BEAVER_LIST_BOX_ROW_H
#define BEAVER_LIST_BOX_ROW_H

#include <gtk/gtk.h>

#include "gopher-menu.h"

#define BEAVER_TYPE_LIST_BOX_ROW beaver_list_box_row_get_type()

G_DECLARE_FINAL_TYPE(BeaverListBoxRow, beaver_list_box_row, BEAVER, LIST_BOX_ROW, GtkListBoxRow)

/**
 * Create a new list box row for the given Gopher menu item.
 *
 * The widget owns the Gopher menu item.
 */
GtkWidget *beaver_list_box_row_new(GopherItem *item);

/**
 * Get the Gopher menu item associated with this list box row.
 */
GopherItem *beaver_list_box_row_get_item(BeaverListBoxRow *self);

#endif
