/**
 * Beaver: a GTK4-based Gopher client
 * Copyright (C) 2022 Hunter Turcin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef GOPHER_MENU_H
#define GOPHER_MENU_H

#include <stdbool.h>

enum _GopherItemType
{
    TYPE_FILE = '0',
    TYPE_DIRECTORY = '1',
    TYPE_CSO_PHONEBOOK = '2',
    TYPE_ERROR = '3',
    TYPE_BINHEX = '4',
    TYPE_DOS_ARCHIVE = '5',
    TYPE_UUENCODED = '6',
    TYPE_INDEX_SEARCH = '7',
    TYPE_TELNET = '8',
    TYPE_BINARY = '9',
    TYPE_REDUNDANT_SERVER = '+',
    TYPE_TN3270 = 'T',
    TYPE_GIF = 'g',
    TYPE_IMAGE = 'I',

    TYPE_INFO = 'i',
    TYPE_PNG = 'p',
    TYPE_HTML = 'h',
    TYPE_DOCUMENT = 'd',
    TYPE_SOUND = 's',
    TYPE_MAIL = 'M',
};

/**
 * The type of a Gopher menu item, represented as a single character.
 */
typedef enum _GopherItemType GopherItemType;

struct _GopherItem
{
    /**
     * The type of the menu item.
     */
    GopherItemType type;

    /**
     * The user-readable display name of the menu item.
     */
    const char *display;

    /**
     * The resource the menu item points to.
     */
    const char *selector;

    /**
     * The hostname of the resource the menu item points to.
     */
    const char *hostname;

    /**
     * The port of the resource the menu item points to.
     */
    int port;
};

/**
 * A Gopher menu item.
 */
typedef struct _GopherItem GopherItem;

/**
 * Create an array of Gopher menu items from a raw response.
 *
 * The array is terminated with NULL and stops at the first invalid line.
 */
GopherItem **gopher_item_array_new_from_raw(const char *raw);

/**
 * Create a new Gopher menu item.
 *
 * The menu item creates copies of the passed strings.
 */
GopherItem *gopher_item_new(GopherItemType type, const char *display, const char *selector, const char *hostname, int port);

/**
 * Create a new Gopher menu item.
 *
 * The menu item takes ownership of the passed strings.
 */
GopherItem *gopher_item_new_moving(GopherItemType type, const char *display, const char *selector, const char *hostname, int port);

/**
 * Create a new Gopher menu item from a raw response line.
 *
 * The line should be in the form:
 *   - <type>
 *   - <display>
 *   - tab
 *   - <selector>
 *   - tab
 *   - <hostname>
 *   - tab
 *   - <port>
 *
 * Returns NULL if the line is malformed.
 */
GopherItem *gopher_item_new_from_raw(const char *raw);

/**
 * Destroy a Gopher menu item.
 *
 * The menu item's copies of its strings are destroyed.
 */
void gopher_item_free(GopherItem *item);

/**
 * Return a formatted URI to the destination of this Gopher item.
 *
 * @return a caller-owned string
 */
char *gopher_item_to_uri(GopherItem *self);

/**
 * Determine if this Gopher menu item points to a non-Gopher resource.
 *
 * @return whether or not the menu item points to a non-Gopher resource
 */
bool gopher_item_is_url(GopherItem *self);

#endif
