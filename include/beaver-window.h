/**
 * Beaver: a GTK4-based Gopher client
 * Copyright (C) 2022 Hunter Turcin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef BEAVER_WINDOW_H
#define BEAVER_WINDOW_H

#include <gtk/gtk.h>

#define BEAVER_TYPE_WINDOW beaver_window_get_type()

G_DECLARE_FINAL_TYPE(BeaverWindow, beaver_window, BEAVER, WINDOW, GtkApplicationWindow);

/**
 * Create a new main window for the application.
 */
GtkWidget *beaver_window_new(GtkApplication *app);

#endif
