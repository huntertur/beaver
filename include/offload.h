/**
 * Beaver: a GTK4-based Gopher client
 * Copyright (C) 2022 Hunter Turcin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef OFFLOAD_H
#define OFFLOAD_H

#include <stdlib.h>

/**
 * Open the given data as a file in the user's preferred viewer.
 */
void open_viewer_for_content(const char *filename, const void *data, size_t size);

/**
 * Open the given URL in the user's preferred viewer.
 */
void open_viewer_for_url(char *url);

#endif
